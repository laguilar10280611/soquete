
//es como funcionan los servidores actualmente, se delega la peticon a un hilo, el hilo es quien atiende las peticiones
package programa;
import java.awt.*;
import javax.swing.*;
import java.net.*;
import java.io.*;
// paquete para los sockets

// PAQUETE PARA EL FLUJO DE DATOS


public class Servidor extends JFrame {

   // declaración de los objectos sockets

    ServerSocket server;
    Socket s;
    int clienteNum = 0;
    JTextArea display;
    
    InetAddress dir;

    public Servidor() {
        super("Servidor");
        display = new JTextArea(20, 5);
        add("Center", display);
        setSize(400, 300);
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    }

    public void runServer() {
        String msj;
        try {
            
            // creación de la instancia socket para el cliente
            dir= InetAddress.getByName("127.0.1.1");
            server= new ServerSocket(6000,100,dir);//acepta a 100 clientes, pero afectaria el rendimiento
             //
          
            display.setText("Esperando por un cliente....\n\n");

            do {
                clienteNum++;
                s = server.accept();//instancia de socket y la usams con el mtodo acept
                System.out.println("Atendiendo al cliente:  " + clienteNum);
                display.append("socket cliente : " + s.toString());
                // creación de hilo, creo instancias anonimas, no me interesa dar nombre a los hijos del serv 
                
                new ServidorHilo(display, server, s, clienteNum).start();
            } while (s != null);//mientras haya solicitud de cliente

        } catch (IOException ex) {     }
    }

    public static void main(String args[]) {
        Servidor s = new Servidor();
        s.runServer();
    }
}