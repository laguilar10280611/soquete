package programa;
import java.awt.*;
import java.awt.event.*;
import java.io.IOException;
import javax.swing.*;
import java.net.*;
import java.io.*;


public class Cliente extends JFrame implements ActionListener{

    Container c;
    JTextField op1,op2;//ok
    JLabel l1,l2,l3;//ok
    JPanel pN,pC,pS;
    JPanel p1,p2,p3,p4;
    JButton boton;//ok
    JTextArea display;
    
    public Cliente()
    {
        setTitle("Cliente de socket");
        setLayout(new GridLayout(3,1));
        c= getContentPane();
        c.setBackground(Color.cyan);
        // creación de las intancias y agregación a los paneles 
        // de las componentes GUI
        op1= new JTextField("",20);
        op2= new JTextField("",20);
        l1= new JLabel("primer sumando");
        l2= new JLabel("segundo sumando");
        //l3=new JLabel("ARMANDO AGUILAR ARRIAGA\n");
        boton= new JButton("Dame SUma");
        display= new JTextArea();
        pC= new JPanel();
        pS= new JPanel();
        pN= new JPanel();
        
//        pC.add(l3);
        pC.add(l1);
        pC.add(op1);
        pC.add(l2);
        pC.add(op2);
        pC.add(boton);
        pS.add(display);
        pN.add(boton);
        
        c.add(pC);
        c.add(pN);
        c.add(pS); 
        
        
        
          pack();
        this.setLocationRelativeTo(null);
        this.setSize(600,600);
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        boton.addActionListener(this);
        
        
        
        
        
        //creo un Form
        //...campos para ingresar primer sumando segundo sumando y boton: servidor dame suma y abajo el textarea
    }         

 @Override
    public void actionPerformed(ActionEvent e) {
       double s1,s2;    //solicito los dobles a mi usurario
    	s1=Double.parseDouble(op1.getText());
    	s2=Double.parseDouble(op2.getText());
    	System.out.println("Haciendo petición");
    	peticionServidor(s1,s2);//hago la peticion
    }
    
    private void peticionServidor(double s1, double s2)
    {
        // declaración de un objecto para el socket cliente
   Socket client;
   DataInputStream input;
   DataOutputStream output;
         // declaración de los objetos para el flujo de datos
         
         double suma;    
         String Suma;
         try {
             //el getlocalhost, m regresa una address
                // creación de la instancia del socket
                    client= new Socket(InetAddress.getLocalHost(),6000);
                    
                    
		    display.setText("Socket Creado....\n");
                // creación de las instancias para el flujo de datos
                    input= new DataInputStream(client.getInputStream());
                    output= new DataOutputStream(client.getOutputStream());
                     
	           display.append("Enviando primer sumando\n");
                  output.writeDouble(s1);
		     display.append("Enviando segundo sumando\n");
                  output.writeDouble(s2);
		     display.append ("El servidor dice....\n\n");
                  suma=input.readDouble();
                  Suma= String.valueOf (suma);
	           display.append("El  resultado es: "+ Suma+"\n\n");
                 display.append("Cerrando cliente\n\n");
                 client.close();
              }

                catch(IOException e){
                 e.printStackTrace();
                }
     }
    
    public static void main(String args[])
    {
        new Cliente();
        
    }
}